//1.1
enum Colour: String {
    case blue = "blue"
    case red = "red"
    case orange = "orange"
}
let rainbow = Colour.orange
print(rainbow.rawValue)
//1.2
enum Fruit: String {
    case kiwi = "kiwi"
    case coconut = "coconut"
    case strawberry = "strawberry"
}
let eat = Fruit.strawberry
print(eat.rawValue)
//2
enum sex {
case male
case female
}
enum age
{
  case child
  case teenager
  case young adult
  case adult
}
enum experience
{
  case yes
  case no
}
//3
enum rainbow {
  case red 
  case orange 
  case yellow
  case green 
  case blue 
  case indigo
  case violet
}
//4
enum rainbow {
  case red 
  case orange 
  case green 
}
var colour = rainbow.red
switch colour {
  case .red:
    print("red apple")
case .orange:
    print("orange lemon")
case .green:
    print("green strawberry")
}
//5
enum estimate {
  case two
  case three
  case four
  case five
}
var people = ["peopleOne", "peopleTwo", "peopleThree", "PeopleFour"]
func book (score: estimate, student:String)
{
  switch score {
    case.two:
    print("Оценка 2 -\(student)")
    case.three:
    print("Оценка 3 -\(student)")
    case.four:
    print("Оценка 4 -\(student)")
    case.five:
    print("Оценка 5 -\(student)")
  }
}
book(score: .three, student:people [3])
//6
enum cars {
  case toyota
  case renault 
  case lada 
}
var car = cars.lada
switch car {
  case .toyota:
    print("У тебя в гараже тойота")
case .renault:
    print("У тебя в гараже рено")
case .lada:
    print("У тебя в гараже лада")
}
